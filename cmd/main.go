package main

import (
	"fmt"
	"log"

	"gitlab.com/extension_go/pkg/db"
	"gitlab.com/extension_go/storage/mongodb"
)

func main() {
	connectDb, err := db.ConnectToMongoDb()
	if err != nil {
		log.Fatal("Error while connection database: ", err.Error())
	}

	strg := mongodb.NewStorageRepo(connectDb)
	fmt.Println(strg)

}
