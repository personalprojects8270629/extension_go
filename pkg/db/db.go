package db

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func ConnectToMongoDb() (*mongo.Client, error) {
	return mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:27017"))
}
