package repo

import "gitlab.com/extension_go/helper/models"

type ExtentionI interface {
	Create(models.Video) (models.Video, error)
}
