package mongodb

import (
	"context"
	"time"

	"gitlab.com/extension_go/helper/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type StorageRepo struct {
	Db *mongo.Client
}

func NewStorageRepo(db *mongo.Client) *StorageRepo {
	return &StorageRepo{Db: db}
}

func (r StorageRepo) Create(req models.Video) (models.Video, error) {
	collection := r.Db.Database("exentiondb").Collection("videos")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err := collection.InsertOne(ctx, bson.D{
		{
			Key: "id", Value: req.Url,
		},
	})
	if err != nil {
		return models.Video{}, err
	}
	return req, err
}
