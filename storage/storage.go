package storage

import (
	"gitlab.com/extension_go/storage/mongodb"
	"gitlab.com/extension_go/storage/repo"
	"go.mongodb.org/mongo-driver/mongo"
)

type StorageI interface {
	Extention() repo.ExtentionI
}

type StorageDb struct {
	Db            *mongo.Client
	extentionrepo repo.ExtentionI
}

// NewStoragePg
func NewStoragePg(db *mongo.Client) *StorageDb {
	return &StorageDb{
		Db:            db,
		extentionrepo: mongodb.NewStorageRepo(db),
	}
}

func (s StorageDb) Extention() repo.ExtentionI {
	return s.extentionrepo
}
